﻿using ConfiguringApps.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConfiguringApps.Controllers
{
    public class HomeController: Controller
    {
        private readonly UptimeService uptime;
        private readonly ILogger<HomeController> log;

        public HomeController(UptimeService uptime, ILogger<HomeController> log)
        {
            this.uptime = uptime;
            this.log = log;
        }

        public ViewResult Index(bool throwException = false)
        {
            log.LogDebug($"Handled {Request.Path} at uptime {uptime.Uptime}");

            if (throwException)
            {
                throw new System.NullReferenceException();
            }
            return View(new Dictionary<string, string>
            {
                ["Message"] = "This is the Index action",
                ["Uptime"] = $"{uptime.Uptime}ms"
            });
        }
        public ViewResult Error() => View(nameof(Index),
        new Dictionary<string, string>
        {
            ["Message"] = "This is the Error action"
        });
    }
}

